public class aula39 {  // labels em blocos de instrução

    public static void main(String[] args) {
        label1:{
            System.out.println("label 1 ini"); // imprimi msg
            label2:{
                System.out.println("label 2 ini"); // imprimi msg
                label3:{
                    System.out.println("label 3 ini"); // imprimi msg
                    if (true) // verifica
                        break label2; // encerra
                }
                System.out.println("label 2 fim"); // imprimi msg
            }
            System.out.println("label 1 fim"); // imprimi msg
        }

    }
}
